using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Walid.Singleton;
using Walid.UI;

public class AsyncSceneManager : SingletonTemplate<AsyncSceneManager>
{
    public IEnumerator LoadSceneAsync(ProgressBar progressBar, string sceneName, UnityAction<Scene, LoadSceneMode> onSceneLoaded = null)
    {
        if (onSceneLoaded is not null)
        {
            onSceneLoaded += (scene, mode) =>
            {
                SceneManager.sceneLoaded -= onSceneLoaded;
                onSceneLoaded = null;
            };
            SceneManager.sceneLoaded += onSceneLoaded;
        }
        progressBar.gameObject.SetActive(true);
        progressBar.statusText.text = string.Format("Loading \"{0}\" Scene", sceneName);
        //Debug.Log(string.Format("Scene loading - {0}", sceneName));
        yield return new WaitForSeconds(0.5f);
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        asyncOperation.allowSceneActivation = false;
        float progress = 0f;
        while (!asyncOperation.isDone)
        {
            progress = Mathf.Clamp01(asyncOperation.progress / 0.9f);
            progressBar.Value = progress;
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        while (SceneManager.GetActiveScene() != SceneManager.GetSceneByName(sceneName))
        {
            progress += 0.000000001f;
            progressBar.Value = progress;
            yield return new WaitForEndOfFrame();
        }
        progressBar.Reset();
    }
}
